# SATIE

![](images/erik.jpg){width=60%}

# SATIE

... non, pas vraiment ...

# SATIE

Spatial Audio Toolkit for Immersive Environments

# SATIE

* synchronisé avec les moteurs 3D
* interface avec beaucoup d'haut-parleurs
* rendus simultanés
* multitude des sources sonores

# SATIE - pourquoi?

objet sonore

<iframe src="https://player.vimeo.com/video/398346954?h=d4ab3f4b2a#t=0m37s&autoplay=1" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

# SATIE est pour:

* sources sonores:
  * entrées "live"
  * synthèse
* effets

# SATIE - comment?

* OSC (Open Sound Control)
* Applications:
  * Godot Game Engine
  * Unity3D
  * Blender
  * Max(4Live), Pure Data, etc...

# Demo

Here are some examples of SATIE in action:

<iframe src="https://player.vimeo.com/video/616508216?autoplay=1" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
