# Splash

Projection mapping

[https://gitlab.com/sat-metalab/splash](https://gitlab.com/sat-metalab/splash)

# Splash

"Le projection mapping utilise des vidéoprojecteurs, mais plutôt que de projeter sur un écran plat (...), la lumière est projetée sur une surface arbitraire (...)"

-- <cite>[http://projection-mapping.org/what-is-projection-mapping/](http://projection-mapping.org/what-is-projection-mapping/)</cite>

---

### Projection sur des surfaces arbitraires

![](images/realObjectWithVideo.jpg){width=34.4%} ![](images/posture-interaction.png){width=55%}

---

### Projection sur des surface mobiles

<iframe src="https://player.vimeo.com/video/268028595" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

---

### Alignement de la projection sur la surface

![](images/realObjectWireframe.jpg){width=75%}

---

### Prise en compte des zones de chevauchement

![](images/semidome_wireframe_no_blending.jpg){width=75%}
