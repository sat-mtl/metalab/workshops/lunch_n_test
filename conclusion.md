---

## Tour d'horizon des documentations

* [https://sat-metalab.gitlab.io/livepose](https://sat-metalab.gitlab.io/livepose)
* [https://sat-metalab.gitlab.io/satie](https://sat-metalab.gitlab.io/satie)
* [https://sat-metalab.gitlab.io/splash](https://sat-metalab.gitlab.io/splash)

---

## Questions

* Aisance de navigation sur le site
* Clareté de la présentation
* Justesse et pertinence de l'information
* Capacité à trouver les réponses souhaitées

---

Si vous pouviez améliorer une chose, ce serait quoi ?

---

## MERCI !
## THANK YOU!
