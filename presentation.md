---
title:
- Lunch&Test
author:
- Edith Viau, Emmanuel Durand, Gabriel Downs, Michał Seta
institute:
- Société des Arts Technologiques [SAT]
theme:
- default
date:
- 29 septembre 2021
aspectratio:
- 169
navigation:
- empty
section-titles:
- false
output:
  revealjs::revealjs_presentation:
    css: styles.css
---

<object alt="diagram demonstrating the links between research theme and software projects" data="./images/ecosystem.svg" width="60%">
  <img alt="diagram demonstrating the links between research theme and software projects" src="./images/ecosystem.svg" width="60%">
</object>


---

# Logiciels concernés

* LivePose
* SATIE
* Splash

