set -e

if [ ! -d reveal.js ]
then
    wget https://github.com/hakimel/reveal.js/archive/3.9.2.tar.gz
    tar -xzvf 3.9.2.tar.gz
    mv reveal.js-3.9.2 reveal.js
    rm 3.9.2.tar.gz
fi

pandoc -t revealjs --css styles.css --metadata pagetitle="Lunch&Test" \
  -s -o presentation.html -V revealjs-url=./reveal.js \
  --variable transition='none' --variable theme="moon" presentation.md \
  notes/livepose.md notes/satie.md notes/splash.md conclusion.md
